# Wallpapers

My awsome wallpaper collection for Desktops/Laptops and Cellphones. Containing various styles of photos, paintings, drawings and other stuff. Thank you ItsTerm1n4l for your collection, I made a Fork to add more wallpapers that i like...

These are the categories of the wallpapers:

1. Generals
- **All** - All wallpapers.  
- **Favorites** - My favourite wallpapers.
- **Mobile** - Wallpapers for phones.

2. For each theme
- **Abstract** - Abstract wallpapers.
- **Airplanes** - Airplanes wallpapers.
- **Animals** - Animals wallpapers.
- **Cars** - Cars wallpapers.
- **Cosmos** - Wallpapers from the Cosmos.
- **Fantasy** - Fantasy themed wallpapers.
- **Futurist** - Futuristic themed wallpapers.
- **Winter** - Winter wallpapers.
- **Games** - Wallpapers from games.
- **Linux** - Linux related wallpapers.
- **Waves** - Ocean waves wallpapers.
- **Landscapes** - Landscapes wallpapers.
- **Retro** - Wallpapers with Retro/Synthwave style.
- **RGB** - RGB wallpapers.
- **Tech** - Tech related wallpapers.
- **Terror** - Horror themed wallpapers.

3. Especials (Especific Styles or Color Schemes)
- **Dracula** - Wallpapers with the Dracula color scheme.  
- **Nord** - Wallpapers with the Nord color scheme.
- **Gruvbox** - Wallpapers with the Gruvbox color scheme.  
- **Style Firewatch** - Wallpapers that follow the firewatch style - Smooth landscapes.  

## **Disclaimer** 

This is just my collection of wallpapers that i like. **ALL** wallpapers are made by other people, im author of none of these wallpapers.

If you want to add more wallpapers to this repo. Make a commit with your wallpaper and i will add to this collection.

### **Licenças/Licenses**.

MIT: https://lbesson.mit-license.org/  
GPL: https://www.gnu.org/licenses/gpl-3.0.txt  

I dont want to be sued. That's the reason for the licenses.
